using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Libreria;

namespace PaginaAtencionAlCliente
{
    public partial class Login1 : System.Web.UI.Page
    {
        private List<Administrativo> _admin;

        public List<Administrativo> Administrativos
        {
            get
            {
                if (Session["Adminsitrativos"] == null)
                {
                    Session["Administrativos"] = new List<Administrativo>();
                }
                return (List<Administrativo>)Session["Administrativos"];
            }
            set
            {
                Session["Administrativos"] = value;
            }


        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            Administrativo a = new Administrativo();
            try
            {
                a.UserName = txtUser.Text;
            }
            catch(ArgumentException jj)
            {
                txtUser.Text = "";
                txtUser.Focus();
                return;
            }
            try
            {
                a.Password = txtPassword.Text;
            }
            catch(ArgumentException jj)
            {
                txtPassword.Text = "";
                txtPassword.Focus();
                return;
            }
            Administrativos.Add(a);
            Response.Redirect("IngresoCorrecto.aspx");

        }

    }
}